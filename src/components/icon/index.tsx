import React from "react"

type IconProps = {
    className: string
    [key: string]: any
}
const Icon = (props: IconProps) => {
    const iconClass = `iconfont ${props.className}`

    return (
        <i className={iconClass} />
    )
}

export default Icon
