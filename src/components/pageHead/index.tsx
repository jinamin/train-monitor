import React from "react"
import styles from "./index.module.less"

type PageHeadProps = {
    title?: string
}

const PageHead = (props: PageHeadProps) => {
    const { title } = props

    return (
        <div className={styles.pageHead}>
            <div>{title}</div>
        </div>
    )
}

export default PageHead
