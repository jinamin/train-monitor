import React from 'react'
import Layout from "./layoout/index";
import 'antd/dist/antd.css'
import "./App.less"
import { ConfigProvider } from 'antd'
import zhCN from 'antd/es/locale/zh_CN'
import * as dayjs from 'dayjs'
import * as isLeapYear from 'dayjs/plugin/isLeapYear' // import plugin
import 'dayjs/locale/zh-cn' // import locale

dayjs.extend(isLeapYear) // use plugin
dayjs.locale('zh-cn') // use locale

function App() {

  return (
    <div className="App">
        <ConfigProvider locale={zhCN}>
            <Layout />
        </ConfigProvider>
    </div>
  )
}

export default App
