import { v4 as uuidv4 } from 'uuid'

const getUId = () => {
    return uuidv4().split('-')[0]
}

export { getUId }
