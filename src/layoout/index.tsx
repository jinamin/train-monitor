import React, { useState, useEffect } from 'react'
import NavSide from "./navSide";
import styles from "./index.module.less"
import { BrowserRouter as Router } from "react-router-dom"
import Routes from "./routes";

const Layout: React.FC = () => {

    return (
        <div className={styles.layout}>
            <Router>
                <NavSide />
                <div className={styles.container}>
                    <Routes />
                </div>
            </Router>
        </div>
    )
}

export default Layout
