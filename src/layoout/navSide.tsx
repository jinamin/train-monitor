import React, { useState, useEffect } from 'react'
import styles from "./navSide.module.less"
import classNames from "classnames"
import { useHistory, useLocation, withRouter } from "react-router"
import Icon from "../components/icon/index";

type Menu = {
    label: string
    icon: string
    path: string
}
type Menus = Array<Menu>

const NavSide: React.FC = () => {
    const location = useLocation()
    const history = useHistory()

    const menus: Menus = [
        {
            label: "DashBoard",
            icon: "amin-diannao",
            path: "/dashBoard"
        },
        {
            label: "监控页",
            icon: "",
            path: "/home"
        },
        {
            label: "全局监控",
            icon: "amin-jiankong",
            path: "/monitor"
        },
        {
            label: "全局监控",
            icon: "",
            path: ""
        },
        {
            label: "全局监控",
            icon: "",
            path: ""
        },
        {
            label: "全局监控",
            icon: "",
            path: ""
        }
    ]

    const menuClick = (path: string): void => {
        history.push(path)
    }

    return (
        <div className={styles.navSide}>
            <div className={styles.menuBanner}>

            </div>
            {menus.map((item: Menu, idx: number) => {
                return (
                    <div
                        onClick={() => menuClick(item.path)}
                        key={idx}
                        className={classNames(
                            styles.menuItem,
                            item.path && location.pathname.includes(item.path) && styles.menuItemActive
                        )}
                    >
                        <Icon className={classNames(item.icon, styles.menuIcon)}/>
                        {item.label}
                    </div>
                )
            })}
        </div>
    )
}

export default withRouter(NavSide)
