import React from "react"
import { Redirect, Route, Switch, withRouter, useLocation, useRouteMatch } from "react-router";
import Home from "../pages/home/index";
import Log from "../pages/log/index";
import DashBoard from "../pages/dashBoard/index";
import Monitor from "../pages/monitor/index";

const Routes: React.FC = () => {

    return (
        <Switch>
            <Route component={Home} path={"/home"} />
            <Route component={DashBoard} path={"/dashBoard"} />
            <Route component={Log} path={"/log"} />
            <Route component={Monitor} path={"/monitor"} />
            <Redirect to={"/dashBoard"} />
        </Switch>
    )
}

export default withRouter(Routes)
