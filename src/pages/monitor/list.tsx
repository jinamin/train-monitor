import React, { useContext, useEffect } from "react"
import styles from "./index.module.less"
import {monitorContext, Item} from "./index";

type ChunkProps = {
    data: {
        title: string
        children: Array<{label: string, value: number}>
    }
}


type Context = {
    setListValue?: (val: number) => void
    listValue?: number
}

const Chunk = (props: ChunkProps) => {
    const { data } = props
    const { title, children } = data
    const context: Context = useContext(monitorContext)
    const {setListValue} = context

    const chunkClick = (value: number) => {
        console.log(value, "===")
    }

    const dragStart = (listValue: number) => {
        console.log(listValue)
        setListValue && setListValue(listValue)
    }

    return (
        <div className={styles.chunk}>
            <div className={styles.chunkTitle}>{title}</div>
            <div className={styles.items}>
                {children.map(item => {
                    return (
                        <div
                            draggable={true}
                            onDragStart={() => dragStart(item.value)}
                            onClick={() => chunkClick(item.value)}
                            key={item.value}
                            className={styles.chunkItem}
                        >
                            {item.label}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

type ListProps = {
    data: Array<Item>
}

const List = (props: ListProps) => {
    const { data } = props

    return (
        <div className={styles.monitorList}>
            {data.map((item: Item, idx: number) => {
                return <Chunk key={idx} data={item} />
            })}
        </div>
    )
}

export default List
