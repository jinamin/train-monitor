import React from "react"
import styles from "./index.module.less"

const Explain = () => {

    const data = [
        { label: "支持自定义监控类型" },
        { label: "支持自定义监控链路" },
        { label: "支持实时监控" },
        { label: "单点连通性测试" },
        { label: "查看节点运行状态" },
        { label: "放大缩小重定位视图" },
        { label: "..." },
    ]

    return (
        <div className={styles.explainBox}>
            <div className={styles.explainTitle}>全局监控流程图</div>
            {data.map((item: { label: string}, idx: number) => {
                return (
                    <div key={idx}>
                        - {item.label}
                    </div>
                )
            })}
        </div>
    )
}

export default Explain
