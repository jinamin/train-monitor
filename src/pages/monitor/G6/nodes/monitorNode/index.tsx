import React from "react"

const monitorNode = [
	"monitorNode",
	{
		options: {
			style: {},
			stateStyles: {
				hover: {
					stroke: "#1790ff",
					lineWidth: 1,
					lineDash: [2, 2, 2, 2],
					circleTop: {
						stroke: "#1790ff",
						fill: "rgba(23, 144, 255, 0.2)"
					},
					circleBottom: {
						stroke: "#1790ff",
						fill: "rgba(23, 144, 255, 0.2)"
					},
					circleLeft: {
						stroke: "#1790ff",
						fill: "rgba(23, 144, 255, 0.2)"
					},
					circleRight: {
						stroke: "#1790ff",
						fill: "rgba(23, 144, 255, 0.2)"
					},
				}
			},
		},
		draw(model, group) {
			// 定义的其他方法，都可以在draw里面调用， 如 drawShape、drawLabel 等。
			const defaultStyle = {
				width: 160,
				height: 32
			}
			const defaultPoints = [
				[0.5, 0],
				[0.5, 1],
				[0, 0.5],
				[1, 0.5],
			]
			model.anchorPoints = model.anchorPoints || defaultPoints
			const keyShape = group.addShape("rect", {
				attrs: {
					x: -80,
					y: -16,
					width: defaultStyle.width,
					height: defaultStyle.height,
					stroke: "#c9d8ad",
					fill: "#e6f6ee",
					lineWidth: 0.5,
					radius: 4,
					fontSize: 14,
					cursor: 'pointer',

				},
				name: "rect",
				draggable: true
			})

			group.addShape("text", {
				attrs: {
					y: 7,
					fill: "#333",
					fontSize: 14,
					text: model.label,
					fontWeight: 400,
					lineHeight: 32,
					textAlign: "center",
					cursor: 'pointer'
				},
				name: "text1",
				draggable: true
			})

			const circleR = 4
			const circleTop = group.addShape("circle", {
				attrs: {
					x: 0,
					y: -defaultStyle.height / 2,
					r: circleR,
					stroke: 'transparent',
					draggable: true
				},
				name: "circleTop",
				draggable: false
			})

			circleTop.on("mousedown", () => {
				model.setNodeStartPoint && model.setNodeStartPoint(model, 0)
			})
			circleTop.on("mouseup", () => {
				model.setNodeEndPoint && model.setNodeEndPoint(model, 0)
			})

			const circleBottom = group.addShape("circle", {
				attrs: {
					x: 0,
					y: defaultStyle.height / 2,
					r: circleR,
					stroke: 'transparent',
				},
				name: "circleBottom",
				draggable: false
			})
			circleBottom.on("mousedown", () => {
				model.setNodeStartPoint && model.setNodeStartPoint(model, 1)
			})
			circleBottom.on("mouseup", () => {
				model.setNodeEndPoint && model.setNodeEndPoint(model, 1)
			})


			const circleLeft = group.addShape("circle", {
				attrs: {
					x: -defaultStyle.width / 2,
					y: 0,
					r: circleR,
					stroke: 'transparent',
				},
				name: "circleLeft",
				draggable: false
			})
			circleLeft.on("mousedown", () => {
				model.setNodeStartPoint && model.setNodeStartPoint(model, 2)
			})
			circleLeft.on("mouseup", () => {
				model.setNodeEndPoint && model.setNodeEndPoint(model, 2)
			})

			const circleRight = group.addShape("circle", {
				attrs: {
					x: defaultStyle.width / 2,
					y: 0,
					r: circleR,
					stroke: 'transparent',
				},
				name: "circleRight",
				draggable: false
			})
			circleRight.on("mousedown", () => {
				model.setNodeStartPoint && model.setNodeStartPoint(model, 3)
			})
			circleRight.on("mouseup", () => {
				model.setNodeEndPoint && model.setNodeEndPoint(model, 3)
			})


			return keyShape
		},
		update(model, group) {
			console.log(model, group)
		}
	},
	"rect",
]

export default monitorNode;
