import React from 'react'
import G6 from '@antv/g6'
import nodes from './nodes'
import edges from './edges'
import combos from './combos'

nodes.map((item) => {
	// @ts-ignore
	G6.registerNode(...item)
})

edges.map((item) => {
	// @ts-ignore
	G6.registerEdge(...item)
})

combos.map((item) => {
	// @ts-ignore
	G6.registerCombo(...item)
})

export default G6
export * from "@antv/g6"
