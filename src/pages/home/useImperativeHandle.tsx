import React, { useRef, useImperativeHandle, forwardRef, useEffect } from "react"

const Child = forwardRef((props: any, ref: any) => {
    const childRef = useRef<HTMLDivElement>()

    const click = () => {
        console.log(2222)
    }

    useImperativeHandle(ref, () => ({
        click: click,
        childDom: childRef
    }));

    return (
        <div ref={childRef}>
            child
        </div>
    )
})

type CRef = {
    click: () => void
    childDom: HTMLDivElement
}

const Home: React.FC = () => {
    const cRef = useRef<CRef>()

    useEffect(() => {
        console.log(cRef.current)
        if (cRef.current) {
            cRef.current.click && cRef.current.click()
            console.log(cRef.current.childDom)
        }
    } ,[])

    return (
        <div>
            Home
            <Child ref={cRef} />
        </div>
    )
}

export default Home
