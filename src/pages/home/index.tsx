import React, { useRef, useImperativeHandle, forwardRef, useEffect, useState } from "react"
import { Input } from "antd"
import styles from "./index.module.less"
import classNames from "classnames";
import Icon from "../../components/icon/index";

const list = [
    {
        label: "代码测试",
        select: false,
        status: 1,
        id: 1
    },
    {
        label: "测试任务测试任务测试任务测试任务测试任务测试任务测试任务测试任务测试任务",
        select: false,
        status: 2,
        id: 2
    },
    {
        label: "这是一个",
        select: false,
        status: 3,
        id: 3
    },
    {
        label: "天大地大",
        select: false,
        status: 4,
        id: 4
    },
    {
        label: "说什么呢",
        select: false,
        status: 3,
        id: 5
    },
]

const list2 = [
    {
        label: "代码测试",
        select: false,
        status: 1,
        id: 1
    },
    {
        label: "测试任务测试任务测试任务测试任务测试任务测试任务测试任务测试任务测试任务",
        select: false,
        status: 2,
        id: 2
    },
    {
        label: "这是一个",
        select: false,
        status: 3,
        id: 3
    },
    {
        label: "这是一个2",
        select: false,
        status: 4,
        id: 4
    },
    {
        label: "这是一个3",
        select: false,
        status: 3,
        id: 5
    },
]

const Home = (props) => {
    const inputRef = useRef()
    const selectListRef = useRef()
    const { onFocus, onBlur } = props
    const [focusFlag, setFocusFlag] = useState(false)
    const [inputValue, setInputValue] = useState("")
    const [dataList, setDataList] = useState(list)

    const statusOptions = [
        { value: "创建", key: 1 },
        { value: "进行中", key: 2 },
        { value: "已完成", key: 3 },
        { value: "测试", key: 4 },
        { value: "关闭", key: 5 }
    ]

    const taskBugStatus = [
        { value: "创建", key: 1 },
        { value: "修复中", key: 2 },
        { value: "修复完成", key: 3 },
        { value: "验证中", key: 4 },
        { value: "重新打开", key: 5 },
        { value: "完成", key: 6 }
    ]

    useEffect(() => {
        window.addEventListener("click", otherClick)
        return () => {
            window.removeEventListener("click", otherClick)
        }
    }, [])

    const otherClick = (event) => {
        event.stopPropagation()
        event.preventDefault()
        // @ts-ignore
        const flag = inputRef.current.state.focused
        setFocusFlag(flag)
    }

    const getStatusText = (val) => {
        return taskBugStatus.find(item => item.key === val).value
    }

    const selectFocus = (event) => {
        event.preventDefault()
        event.stopPropagation()
        setFocusFlag(true)
        onFocus && onFocus()
    }

    const selectBlur = () => {
        if (inputValue) {
            const match = dataList.filter(item => item.label.toLowerCase().includes(inputValue.toLowerCase()))
            const value = (match && match.length) ? match[0].label : dataList[0].label
            setInputValue(value)
        }

        onBlur && onBlur()
    }

    const selectClick = (event, item) => {
        event.stopPropagation()
        setInputValue(item.label)
        const targetList = list2.filter(i => i.label.toLowerCase().includes(item.label.toLowerCase()))
        setDataList(targetList)
        otherClick(event)
    }

    const inputChange = (event) => {
        const value = event.target.value
        setInputValue(value)
        const targetList = list2.filter(item => item.label.toLowerCase().includes(value.toLowerCase()))
        value ? setDataList(targetList) : setDataList(list)
    }

    console.log(focusFlag)

    return (
        <div className={styles.box}>
            <div className={styles.searchSelect} onClick={event => event.stopPropagation()}>
                <Input
                    value={inputValue}
                    onFocus={selectFocus}
                    onBlur={selectBlur}
                    className={styles.selectInput}
                    onChange={inputChange}
                    ref={inputRef}
                />
                <div
                    onClick={event => event.stopPropagation()}
                    className={classNames(
                        styles.selectList,
                        focusFlag && styles.selectListShow
                    )}
                    ref={selectListRef}
                >
                    {!inputValue && (
                        <div className={styles.lastSaw}>最近查看</div>
                    )}
                    {
                        dataList.map((item) => {
                            return (
                                <div
                                    onClick={(event) => selectClick(event, item)}
                                    key={item.id}
                                    className={styles.selectItem}
                                >
                                    <div className={styles.itemStatus}>
                                        {getStatusText(item.status)}
                                    </div>
                                    <div className={styles.title}>{item.label}</div>
                                    <div className={styles.avatar}/>
                                </div>
                            )
                        })
                    }
                </div>

            </div>
        </div>
    )
}

export default Home
