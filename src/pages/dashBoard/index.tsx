import React from "react"
import PageHead from "../../components/pageHead/index";
import DashCard from "./card";
import styles from "./index.module.less"
import DashChart from "./chart";

const DashBoard: React.FC = () => {
    const cardData = {
        title: "前台访问量",
        times: 1200,
        weekNum: 12,
        dayNum: -5,
        visitNum: 50
    }

    return (
        <div>
            <PageHead title={"Dashboard"}/>
            <div className={styles.cards}>
                <DashCard data={cardData}/>
                <DashCard data={cardData}/>
                <DashCard data={cardData}/>
            </div>
            <DashChart id={"dashChart"} />
        </div>
    )
}

export default DashBoard
