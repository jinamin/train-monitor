import React, { PropsWithChildren } from "react"
import styles from "./card.module.less"
import Icon from "../../components/icon/index";
import classNames from "classnames"

type DashCardProps = {
    data: {
        title: string
        times: number
        weekNum: number
        dayNum: number
        visitNum: number
    }
} | PropsWithChildren<any>

const DashCard = (props: DashCardProps) => {

    const { data } = props
    const { title, times, weekNum, dayNum, visitNum } = data

    return (
        <div className={styles.dashCard}>
            <div className={styles.title}>
                {title}
            </div>
            <div className={styles.timesBox}>
                <span className={styles.times}>{times}</span>
                次
            </div>
            <div className={styles.compare}>
                周同比
                &nbsp;
                {weekNum}%
                &nbsp;
                <Icon
                    className={classNames(
                        "amin-shangxiajiantou",
                        weekNum > 0 ? styles.upIcon : styles.downIcon
                    )}
                />
                &nbsp;&nbsp;&nbsp;&nbsp;
                日同比
                &nbsp;
                {dayNum}%
                &nbsp;
                <Icon
                    className={classNames(
                        "amin-shangxiajiantou",
                        dayNum > 0 ? styles.upIcon : styles.downIcon
                    )}
                />
            </div>
            <div className={styles.visit}>
                日均访问量 {visitNum} 次
            </div>

        </div>
    )
}

export default DashCard
