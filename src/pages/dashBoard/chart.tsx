import React, { useEffect, useLayoutEffect, useState, useRef } from "react"
import { Chart } from '@antv/g2';
import styles from "./chart.module.less"
import TabBar from "./tabBar";

const data = [
    { month: '1月', value: 30 },
    { month: '2月', value: 90 },
    { month: '3月', value: 80 },
    { month: '4月', value: 90 },
    { month: '5月', value: 90 },
    { month: '6月', value: 50 },
    { month: '7月', value: 60 },
    { month: '8月', value: 30 },
    { month: '9月', value: 10 },
    { month: '10月', value: 55 },
    { month: '11月', value: 65 },
    { month: '12月', value: 40 }
];

type DashChartProps = {
    id: string
}

const DashChart = (props: DashChartProps) => {
    const { id } = props
    const chartRef = useRef()

    const [lineChart, setLineChart] = useState<Chart>(null)
    const [tabKey, setTabKey] = useState<string>("1")

    useLayoutEffect(() => {
        const chart = new Chart({
            container: id,
            autoFit: true,
            height: 500,
        });
        setLineChart(chart)
        chart.data(data);
        chart.scale('value', {
            alias: '销售额(万)',
            nice: true,
        });
        chart.axis('month', {
            tickLine: null
        });

        chart.tooltip({
            showMarkers: false
        });
        chart.interaction('active-region');

        chart.interval().position('month*value')
            .style('month', val => {
                return {
                    fillOpacity: 1,
                    lineWidth: 0,
                    stroke: '#636363',
                    lineDash: [3, 2]
                };
            });

        chart.render();
    }, [])

    const tabChange = (key: string) => {
        console.log(key)
    }

    const list = [
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
        { value: "z阿斯顿发阿斯顿将开放" },
    ]

    return (
        <div>
            <TabBar callback={tabChange} />
            <div className={styles.content}>
                <div ref={chartRef} id={id} className={styles.chartContainer} />
                <div>
                    <div>门店销售额排名</div>
                    {list.map(item => {

                    })}
                </div>
            </div>
        </div>

    )
}

export default DashChart
