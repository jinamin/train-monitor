import React, { useState } from "react"
import styles from "./chart.module.less"
import classNames from "classnames"
import { DatePicker } from "antd"
import * as dayjs from 'dayjs'
import moment, { Moment } from "moment"

const { RangePicker } = DatePicker

const tabs = [
    { label: "销售额", key: "1" },
    { label: "访问量", key: "2" },
]

const timeTabs = [
    { label: "今日", key: "1" },
    { label: "本周", key: "2" },
    { label: "本月", key: "3" },
    { label: "本年", key: "4" },
]

type TabBarProps = {
    callback: (key: string) => void
}

const TabBar = (props: TabBarProps) => {
    const { callback } = props
    const [tabKey, setTabKey] = useState<string>("1")
    const [timeTabKey, setTimeTabKey] = useState<string>("1")
    const [rangeDates, setRangeDates] = useState<[Moment, Moment]>(null)

    const tabChange = (key) => {
        setTabKey(key)
        callback && callback(key)
    }

    const timeTabChange = (key) => {
        setTimeTabKey(key)
    }

    const rangeChange = (dates) => {
        setRangeDates(dates)
    }

    return (
        <div className={styles.tabBar}>
            <div className={styles.tabBarLeft}>
                {tabs.map(item => {
                    return (
                        <div
                            key={item.key}
                            onClick={() => tabChange(item.key)}
                            className={classNames(
                                styles.tab,
                                tabKey === item.key && styles.activeTab
                            )}
                        >
                            {item.label}
                        </div>
                    )
                })}
            </div>
            <div className={styles.timeBar}>
                {timeTabs.map(item =>  {
                    return (
                        <div
                            key={item.key}
                            onClick={() => timeTabChange(item.key)}
                            className={classNames(
                                styles.timeTab,
                                timeTabKey === item.key && styles.activeTimeTab,
                                rangeDates && styles.timeTabDis
                            )}
                        >
                            {item.label}
                        </div>
                    )
                })}
                <RangePicker size={"small"} onChange={rangeChange}/>
            </div>
        </div>
    )
}

export default TabBar

// const dateJudge = (dates) => {
//     if (!dates) return "0"
//     const startTime = dayjs(dates[0]).startOf("day").valueOf()
//     const endTime = dayjs(dates[1]).endOf("day").valueOf()
//
//     const todayStart = dayjs(Date.now()).startOf("day").valueOf()
//     const todayEnd = dayjs(Date.now()).endOf("day").valueOf()
//     const weekStart = dayjs(Date.now()).startOf("week").valueOf()
//     const weekEnd = dayjs(Date.now()).endOf("week").valueOf()
//     const monthStart = dayjs(Date.now()).startOf("month").valueOf()
//     const monthEnd = dayjs(Date.now()).endOf("month").valueOf()
//     const yearStart = dayjs(Date.now()).startOf("year").valueOf()
//     const yearEnd = dayjs(Date.now()).endOf("year").valueOf()
//
//     if (startTime === todayStart && endTime === todayEnd) return "1" // 今日
//     if (startTime >= weekStart && endTime <= weekEnd) return "2" // 本周
//     if (startTime >= monthStart && endTime <= monthEnd) return "3" // 本月
//     if (startTime >= yearStart && endTime <= yearEnd) return "4" // 本年
//     return "0"
// }
